# frozen_string_literal: true

require_relative '../triage'

module Triage
  class ChangedFileList
    OLD_NEW_PATHS = %w[old_path new_path].freeze

    def initialize(project_id, merge_request_iid)
      @project_id = project_id
      @merge_request_iid = merge_request_iid
    end

    def any_change?(matcher)
      merge_request_changes.any? { |change| any_change_from_old_or_new?(change, matcher) }
    end

    def only_change?(matcher)
      merge_request_changes.all? { |change| any_change_from_old_or_new?(change, matcher) }
    end

    def each_change(matcher = //)
      merge_request_changes.each do |change|
        yield(change) if any_change_from_old_or_new?(change, matcher)
      end
    end

    def merge_request_changes
      @merge_request_changes ||= Triage.api_client.merge_request_changes(project_id, merge_request_iid).changes
    end

    private

    attr_reader :project_id, :merge_request_iid

    def any_change_from_old_or_new?(change, matcher)
      OLD_NEW_PATHS.any? do |path|
        case matcher
        when Regexp
          change[path].match?(matcher)
        when String, Array
          Array(matcher).any? { |path_start| change[path].start_with?(path_start) }
        else
          raise ArgumentError, "Unknown argument type `#{matcher.class}`. Accept `Regexp`, `String`, or `Array<String>`!"
        end
      end
    end
  end
end
