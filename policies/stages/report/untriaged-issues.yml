.common_conditions: &common_conditions
  date:
    attribute: created_at
    condition: newer_than
    interval_type: months
    interval: 3
  state: opened
  author_member:
    source: group
    condition: not_member_of
    source_id: 9970
  ruby: |
    resource[:issue_type] == 'issue' &&
      author != 'gitlab-qa' &&
      untriaged?

.common_rules: &common_rules
  limits:
    most_recent: 25

.common_actions: &common_actions
  summarize:
    destination: gitlab-org/quality/triage-reports
    item: |
      - [ ] #{full_resource_reference} {{title}} {{labels}}
    title: |
      #{Date.today.iso8601} Untriaged issues requiring initial triage
    summary: |
      Hi Triage Team,

      Here is a list of the issues that do not meet the triaged criteria in accordance with the
      [Partial triage guidelines](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#partial-triage), we would like to ask you to follow the [wider community issue triaging process](https://about.gitlab.com/handbook/engineering/quality/contributor-success/#community-issues-workflow-manual-process) as outlined in our handbook.

      For the issues triaged please check off the box in front of the given issue.

      Once you've triaged all the issues assigned to you, you can unassign and unsubscribe yourself via these quick actions:

      ```
      /done
      /unassign me
      /unsubscribe
      ```

      **When all the checkboxes are done, close the issue, and celebrate!** :tada:

      #{ triagers = untriaged_issues_quality_cc_triagers.map { |triager| "#{triager}"}; nil }
      #{ distribute_and_display_items_per_triager(resource[:items].lines(chomp: true).last(triagers.size * 4), triagers) }

      ---

      Job URL: #{ENV['CI_JOB_URL']}

      This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/untriaged-issues.yml)

      /label ~Quality ~"triage report"
resource_rules:
  issues:
    rules:
      - name: Collate untriaged issues
        <<: *common_rules
        conditions:
          <<: *common_conditions
          labels:
            - automation:self-triage-encouraged
        actions:
          <<: *common_actions
      - name: Encourage the self triage of untriaged issues
        <<: *common_rules
        conditions:
          <<: *common_conditions
          forbidden_labels:
            - automation:self-triage-encouraged
        actions:
          comment: |
            Hey {{author}}, thank you for creating this issue!

            To get the right eyes on your issue more quickly, we encourage you to follow the [community issue workflow](https://about.gitlab.com/handbook/engineering/quality/contributor-success/#community-issues-workflow-manual-process).

            **PLEASE NOTE**: You will need to use the [`@gitlab-bot label command`](https://about.gitlab.com/handbook/engineering/quality/triage-operations/#reactive-label-command)
            to apply labels to this issue.

            To set expectations, GitLab product managers or team members can't make any promise if they will proceed with this.
            However, we believe [everyone can contribute](https://about.gitlab.com/company/mission/#everyone-can-contribute),
            and welcome you to work on this proposed change, feature or bug fix.
            There is a [bias for action](https://about.gitlab.com/handbook/values/#bias-for-action),
            so you don't need to wait. Try and spin up that merge request yourself.

            If you need help doing so, we're always open to [mentor you](https://about.gitlab.com/community/contribute/mentor-sessions/)
            to drive this change.
            /label ~automation:self-triage-encouraged
